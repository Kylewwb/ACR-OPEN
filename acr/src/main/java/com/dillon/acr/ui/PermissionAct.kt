package com.dillon.acr.ui


import android.Manifest
import android.content.Intent
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.blankj.utilcode.constant.PermissionConstants
import com.blankj.utilcode.util.*
import com.dillon.acr.R
import com.dillon.acr.base.BAct
import com.dillon.acr.services.task.CheckAccessibilitySer
import com.dillon.acr.services.task.RecorderSer
import com.dillon.acr.ui.home.HomeAct
import com.dillon.acr.utils.AUtils
import com.dillon.acr.utils.LocalCacheUtils
import com.dillon.acr.utils.view.shimmer.Shimmer
import kotlinx.android.synthetic.main.activity_permission.*
import kotlinx.android.synthetic.main.layout_common_title.*


class PermissionAct : BAct(), View.OnClickListener {
    private var materialDialog: MaterialDialog? = null

    override fun initUi() {
        super.initUi()
        setContentView(R.layout.activity_permission)
    }

    override fun onResume() {
        super.onResume()
        checkAccessibilityPermissions()

        if (PermissionUtils.isGranted(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.RECORD_AUDIO
            )
        ) {
            AUtils.ignoreBatteryOptimization(this@PermissionAct)
        }

        if (AUtils.checkIgnoreBatteryOptimization() && AUtils.isAccessibilitySettingsOn(RecorderSer::class.java)) {
            startActivity(Intent(this, HomeAct::class.java))
            finish()
        }
    }


    override fun initView() {
        tv_title_center.text = AppUtils.getAppName().toString()

        val shimmer = Shimmer()
        shimmer.setDuration(6000)
        shimmer.start(tv_title_center)
    }


    override fun initData() {
        checkBasePermissions()
    }


    override fun initListener() {
        super.initListener()
        lay_accessibility_permissions.setOnClickListener(this)
        cb_accessibility_permissions.setOnClickListener(this)
        lay_title_center.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v) {
            lay_title_center -> {
                val mTitle = AppUtils.getAppName() + "  v" + AppUtils.getAppVersionName()
                val mMessage = resources.getString(R.string.app_des)
                materialDialog?.cancel()
                materialDialog = MaterialDialog(this)
                    .icon(R.mipmap.ic_logo)
                    .title(text = mTitle)
                    .message(text = mMessage)
                    .cornerRadius(res = R.dimen.dp_20)
                if (materialDialog != null && !materialDialog!!.isShowing) {
                    materialDialog?.show()
                }
            }
            lay_accessibility_permissions, cb_accessibility_permissions -> {
                requestAccessibilityPermissions()
            }

        }
    }

    private fun checkBasePermissions() {
        if (!PermissionUtils.isGranted(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.RECORD_AUDIO
            )
        ) {

            PermissionUtils.permission(
                PermissionConstants.STORAGE,
                PermissionConstants.CONTACTS,
                PermissionConstants.PHONE,
                PermissionConstants.MICROPHONE
            )

                .callback(object : PermissionUtils.FullCallback {
                    override fun onGranted(permissionsGranted: List<String>) {
                        LogUtils.d("onGranted")
                    }

                    override fun onDenied(
                        permissionsDeniedForever: List<String>,
                        permissionsDenied: List<String>
                    ) {
                        LogUtils.d("onDenied")

                        if (permissionsDenied.isNotEmpty() && permissionsDeniedForever.isNullOrEmpty()) {
                            checkBasePermissions()
                        }

                        if (permissionsDenied.isNotEmpty() && !permissionsDeniedForever.isNullOrEmpty()) {
                            val mMessage = getString(R.string.deny_forever_tip)
                            materialDialog?.cancel()
                            materialDialog = MaterialDialog(this@PermissionAct)
                                .message(text = mMessage)
                                .cornerRadius(res = R.dimen.dp_20)
                                .positiveButton(null, getString(R.string.go)) {
                                    AUtils.goAppSetting(this@PermissionAct)
                                }
                            materialDialog?.cancelOnTouchOutside(false)
                            if (materialDialog != null && !materialDialog!!.isShowing) {
                                materialDialog?.show()
                            }
                        }


                    }
                })
                .request()

        }
    }


    private fun checkAccessibilityPermissions() {
        if (AUtils.isAccessibilitySettingsOn(RecorderSer::class.java)
        ) {
            cb_accessibility_permissions.isChecked = true
            cb_accessibility_permissions.isEnabled = false
        } else {
            cb_accessibility_permissions.isChecked = false
            cb_accessibility_permissions.isEnabled = true
        }

    }

    private fun requestAccessibilityPermissions() {
        if (!AUtils.isAccessibilitySettingsOn(RecorderSer::class.java)) {
            AUtils.startCommonService(CheckAccessibilitySer::class.java)
            AUtils.openAccessibility()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        materialDialog?.cancel()
        AUtils.stopCommonService(CheckAccessibilitySer::class.java)

    }
}
