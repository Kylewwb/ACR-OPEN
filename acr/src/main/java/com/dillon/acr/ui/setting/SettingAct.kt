package com.dillon.acr.ui.setting

import android.annotation.SuppressLint
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import com.blankj.utilcode.util.*
import com.dillon.acr.R
import com.dillon.acr.base.App
import com.dillon.acr.base.BAct
import com.dillon.acr.utils.AUtils
import com.dillon.acr.utils.LocalCacheUtils
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.layout_common_title2.*


class SettingAct : BAct(){
    private var materialDialog: MaterialDialog? = null

    override fun initUi() {
        super.initUi()
        setContentView(R.layout.activity_setting)
    }

    override fun initView() {
        super.initView()
        tv_title_left.text = "←"
        lay_title_left.setOnClickListener {
            finish()
        }

        tv_title_center.text = getString(R.string.setting)
        tv_title_right.setTextColor(ColorUtils.getColor(R.color.gold))
        tv_title_right.setOnClickListener {
        }
        lay_title_center.setOnClickListener {
            val mTitle = AppUtils.getAppName() + "  v" + AppUtils.getAppVersionName()
            val mMessage = resources.getString(R.string.app_des)
            materialDialog?.cancel()
            materialDialog = MaterialDialog(this)
                .icon(R.mipmap.ic_logo)
                .title(text = mTitle)
                .message(text = mMessage)
                .cornerRadius(res = R.dimen.dp_20)
            if (materialDialog != null && !materialDialog!!.isShowing) {
                materialDialog?.show()
            }
        }

    }

    override fun initData() {

        tv_file_clean.setOnClickListener {
            val mTitle = getString(R.string.dialog_common_title)
            val mMessage = getString(R.string.clean_tips)
            materialDialog?.cancel()
            materialDialog = MaterialDialog(this)
                //.icon(R.mipmap.ic_launcher)
                .title(text = mTitle)
                .message(text = mMessage)
                .cornerRadius(res = R.dimen.dp_20)
                .negativeButton(null, getString(R.string.clean)) {
                    cleanFileData()
                }
                .positiveButton(null, getString(R.string.cancel)) {
                }
            if (materialDialog != null && !materialDialog!!.isShowing) {
                materialDialog?.show()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        LogUtils.d("onResume")
        tv_title_right.text = LocalCacheUtils.getProExp().toString() + getString(R.string.Exp)
        showFileData()
    }

    override fun onDestroy() {
        super.onDestroy()
        materialDialog?.cancel()

    }

    private fun cleanFileData() {
        Thread {
            val success = FileUtils.deleteAllInDir(App.saveAudioDir)
            if (success) {
                ToastUtils.showShort(getString(R.string.done))
            } else {
                ToastUtils.showShort(getString(R.string.failed))
            }
            ViewUtils.runOnUiThread {
                showFileData()
            }

        }.start()

    }

    @SuppressLint("SetTextI18n")
    private fun showFileData() {
        tv_file_count.text =
            getString(R.string.audio_count) + FileUtils.listFilesInDir(App.saveAudioDir).size
        tv_file_size.text = getString(R.string.size) + FileUtils.getSize(App.saveAudioDir)

    }

}

