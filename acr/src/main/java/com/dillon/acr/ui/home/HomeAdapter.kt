package com.dillon.acr.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.*
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.dillon.acr.R
import com.dillon.acr.bean.MediaInfo
import com.dillon.acr.key.CommonK
import com.dillon.acr.utils.GlideRoundedCornersTransform
import com.dillon.acr.utils.view.shimmer.Shimmer
import com.dillon.acr.utils.view.shimmer.ShimmerTextView

class HomeAdapter(context: Context, itemManagerListener: ItemManagerListener) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    private var mContext = context
    private var mItemManagerListener = itemManagerListener
    private var mDataList: MutableList<MediaInfo>? = null
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getItemCount(): Int {
        return if (mDataList == null) 0 else mDataList!!.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemView = inflater.inflate(R.layout.layout_recycler_item, parent, false)
        return ViewHolder(itemView)

    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val mediaInfo = mDataList!![position]
        holder.setData(mediaInfo)

        holder.ivSave.setOnClickListener { mItemManagerListener.save(mediaInfo, position) }
        holder.ivLoved.setOnClickListener { mItemManagerListener.love(mediaInfo, position) }
        holder.ivShare.setOnClickListener { mItemManagerListener.share(mediaInfo, position) }
        holder.ivDelete.setOnClickListener { mItemManagerListener.delete(mediaInfo, position) }


    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivIcon: ImageView = itemView.findViewById(R.id.iv_icon)
        var ivPlay: ImageView = itemView.findViewById(R.id.iv_play)
        var tvTitle: ShimmerTextView = itemView.findViewById(R.id.tv_title)
        var mSize: TextView = itemView.findViewById(R.id.tv_size)
        var ivLoved: ImageView = itemView.findViewById(R.id.iv_loved)
        var ivShare: ImageView = itemView.findViewById(R.id.iv_share)
        var ivSave: ImageView = itemView.findViewById(R.id.iv_save)
        var ivDelete: ImageView = itemView.findViewById(R.id.iv_delete)


        fun setData(mediaInfo: MediaInfo) {
            tvTitle.text = mediaInfo.title
            mSize.text = mediaInfo.sizeStr
            val loved = SPUtils.getInstance().getBoolean(mediaInfo.file!!.name, false)
            if (loved) {
                Glide.with(Utils.getApp().baseContext).load(R.drawable.icon_loved).into(ivLoved)
            } else {
                Glide.with(Utils.getApp().baseContext).load(R.drawable.icon_love).into(ivLoved)
            }
            val shimmer = Shimmer()
            shimmer.setDuration(7000)
            shimmer.start(tvTitle)


            ivPlay.visibility = View.INVISIBLE
            val options = RequestOptions()
                .error(R.drawable.icon_about) //加载失败图片
                .priority(Priority.HIGH) //优先级
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .transform(
                    GlideRoundedCornersTransform(
                        Utils.getApp().baseContext.resources.getDimension(
                            R.dimen.dp_8
                        ), GlideRoundedCornersTransform.CornerType.ALL
                    )
                )
            Glide.with(Utils.getApp().baseContext).load(R.drawable.icon_audio).apply(options)
                .into(ivIcon)
        }

    }

    fun notifyDataSetChanged(dataList: MutableList<MediaInfo>?) {
        mDataList = dataList
        super.notifyDataSetChanged()
    }


    interface ItemManagerListener {
        fun love(mediaInfo: MediaInfo, position: Int)
        fun share(mediaInfo: MediaInfo, position: Int)
        fun save(mediaInfo: MediaInfo, position: Int)
        fun delete(mediaInfo: MediaInfo, position: Int)
    }
}