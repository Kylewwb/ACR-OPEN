package com.dillon.acr.receivers

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import com.blankj.utilcode.util.ActivityUtils
import com.dillon.acr.base.App
import com.dillon.acr.key.ActionK
import com.dillon.acr.services.task.RecorderSer
import com.dillon.acr.ui.home.HomeAct
import com.dillon.acr.utils.AUtils
import com.dillon.acr.utils.LocalCacheUtils


/**
 * @author dillon
 * @description:
 * @date :2019/8/22 16:30
 */
class CallRec : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            Intent.ACTION_NEW_OUTGOING_CALL -> {
                val outCallNum = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)
                if (checkNumberOk(outCallNum)) {
                    var tempName = outCallNum
                    Thread {
                        tempName = AUtils.getPersonNameByPhone(outCallNum)
                        if (!AUtils.isValidFileName(tempName)) {
                            tempName = outCallNum
                        }
                    }.start()
                    Handler().postDelayed({
                        App.number = "1_$tempName"
                        App.capturingAudio = true
                        startRecord()
                    }, 500)
                }
            }

            else -> {
                val tm = context.getSystemService(Service.TELEPHONY_SERVICE) as TelephonyManager
                tm.listen(listener, PhoneStateListener.LISTEN_CALL_STATE)
            }
        }


    }

    private var listener: PhoneStateListener = object : PhoneStateListener() {
        override fun onCallStateChanged(
            state: Int,
            incomingNumber: String?
        ) {
            super.onCallStateChanged(state, incomingNumber)
            when (state) {
                TelephonyManager.CALL_STATE_IDLE -> {
                    if (checkNumberOk(incomingNumber)) {
                        App.capturingAudio = false
                        endRecord()
                    }

                }
                TelephonyManager.CALL_STATE_OFFHOOK -> {
                    if (checkNumberOk(incomingNumber)) {
                        if (!App.isOutGoingCall) {
                            var tempName = incomingNumber
                            Thread {
                                tempName = AUtils.getPersonNameByPhone(incomingNumber)
                                if (!AUtils.isValidFileName(tempName)) {
                                    tempName = incomingNumber
                                }
                            }.start()
                            Handler().postDelayed({
                                App.number = "0_$tempName"
                                App.capturingAudio = true
                                startRecord()

                            }, 500)

                        }

                    }
                }
                TelephonyManager.CALL_STATE_RINGING ->  // 输出来电号码
                {
                    App.isOutGoingCall = false
                }
            }
        }
    }


    private fun startRecord() {
        AUtils.startCommonService(RecorderSer::class.java)
    }

    private fun endRecord() {
        AUtils.startCommonService(RecorderSer::class.java)
    }

    fun checkNumberOk(paramString: String?): Boolean {
        if (paramString == null) return false
        return !((paramString.startsWith("*") || paramString.startsWith("#")) && paramString.endsWith(
            "#"
        ))
    }
}