package com.dillon.acr.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import com.blankj.utilcode.util.LogUtils


/**
 * @author dillon
 * @description:
 * @date :2019/8/22 16:30
 */
class ARec : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent) {

        when (intent.action) {

            ACTION_BOOT_COMPLETED -> {
                LogUtils.v("BOOT_COMPLETED")
            }

            ACTION_SHUTDOWN -> {
                LogUtils.v("SHUTDOWN")
            }

            ACTION_USER_PRESENT -> {
                LogUtils.v("USER_PRESENT")
            }

            ACTION_SCREEN_ON -> {
                LogUtils.v("SCREEN_ON")

            }

            ACTION_SCREEN_OFF -> {
                LogUtils.v("SCREEN_OFF")

            }
        }
    }

}