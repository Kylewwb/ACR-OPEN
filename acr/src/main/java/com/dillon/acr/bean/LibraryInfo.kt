package com.dillon.acr.bean

class LibraryInfo() : BInfo() {
    var res: Int = 0
    var title: String? = null

    fun getInfo(resId: Int, titleStr: String): LibraryInfo {
        res = resId
        title = titleStr
        return this
    }

}