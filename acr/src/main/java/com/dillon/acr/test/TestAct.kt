package com.dillon.acr.test

//import com.devbrackets.android.exomedia.listener.OnPreparedListener

import cn.jzvd.Jzvd
import com.blankj.utilcode.util.FileUtils
import com.dillon.acr.R
import com.dillon.acr.base.App
import com.dillon.acr.base.BAct



class TestAct : BAct() {
    override fun initUi() {
        super.initUi()
        setContentView(R.layout.activity_test)
    }

    override fun initView() {
        super.initView()
        //setupVideoView()

    }

    override fun initData() {
        super.initData()

    }

    // private fun setupVideoView() {
    val videoList = FileUtils.listFilesInDir(App.saveAudioDir)
//        if(!videoList.isNullOrEmpty()){
//            videoView.setOnPreparedListener(this)
//            //  videoView.setVideoURI(Uri.parse("https://live-s3m.mediav.com/nativevideo/cd32c291194dbab6e730a942fb9075f3-bit_cloud512.mp4"))
//            videoView.setVideoPath(videoList[0].path)
//        }

    //  }


//    override fun onPrepared() {
//        videoView.start()
//
//    }

    override fun onBackPressed() {
        if (Jzvd.backPress()) {
            return
        }
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        Jzvd.releaseAllVideos()
    }

}

