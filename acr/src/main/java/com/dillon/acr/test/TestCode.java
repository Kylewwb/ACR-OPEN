package com.dillon.acr.test;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;

import com.blankj.utilcode.util.Utils;

public class TestCode {

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //要做的事情
            handler.postDelayed(this::run, 2000);
        }
    };


    public void simpleEncrypt(String str) {
        StringBuilder s = new StringBuilder();
        char[] c = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
            int a = c[i];
            a = a + 7;
            s.append((char) a);
        }
    }

    public void simpleDecrypt(String str) {
        StringBuilder d = new StringBuilder();
        char[] c2 = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
            int b = c2[i];
            b = b - 7;
            d.append((char) b);
        }
    }

    public void openAudioManager() {
        try {
            AudioManager audioManager = (AudioManager) (Utils.getApp().getSystemService(Context.AUDIO_SERVICE));
            if (audioManager != null)
                audioManager.setParameters("INCALL_RECORDING_MODE=ON");
        } catch (Exception exception) {
            exception.getStackTrace();
        }
        try {
            AudioManager audioManager = (AudioManager) (Utils.getApp().getSystemService(Context.AUDIO_SERVICE));
            if (audioManager != null) {
                audioManager.setParameters("VOICE_RECORDING_MODE=ON");
                return;
            }
        } catch (Exception exception) {
            exception.getStackTrace();
        }
    }

    public void closeAudioManager() {
        try {
            AudioManager audioManager = (AudioManager) (Utils.getApp().getSystemService(Context.AUDIO_SERVICE));
            if (audioManager != null)
                audioManager.setParameters("INCALL_RECORDING_MODE=OFF");
        } catch (Exception exception) {
            exception.getStackTrace();
        }
        try {
            AudioManager audioManager = (AudioManager) (Utils.getApp().getSystemService(Context.AUDIO_SERVICE));
            if (audioManager != null) {
                audioManager.setParameters("VOICE_RECORDING_MODE=OFF");
                return;
            }
        } catch (Exception exception) {
            exception.getStackTrace();
        }
    }
}
