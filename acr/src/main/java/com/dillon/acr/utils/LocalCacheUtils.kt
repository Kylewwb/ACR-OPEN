package com.dillon.acr.utils

import com.dillon.acr.base.App
import com.dillon.acr.bean.UserInfo
import com.dillon.acr.key.CacheK
import com.blankj.utilcode.util.SPUtils
import com.dillon.acr.key.CommonK


object LocalCacheUtils {


    fun saveLocalUser(userInfo: UserInfo): Boolean {
        App.localUserInfo = userInfo
        val userJson = GsonUtils.gsonString(userInfo)
        if (!userJson.isNullOrBlank()) {
            SPUtils.getInstance().put(
                CacheK.localUserCache,
                userJson
            )
            return true
        }
        return false
    }

    fun getLocalUser(): UserInfo? {
        var localUser: UserInfo? = App.localUserInfo
        if (localUser != null) {
            return App.localUserInfo
        }
        val localUserJson =
            SPUtils.getInstance().getString(CacheK.localUserCache)
        if (!localUserJson.isNullOrBlank()) {
            localUser = GsonUtils.jsonToBean(
                localUserJson,
                UserInfo::class.java
            )
        }
        App.localUserInfo = localUser
        return App.localUserInfo
    }


    fun saveCameraType(cameraType: String) {
        App.cameraType = cameraType
        SPUtils.getInstance().put(CacheK.useCameraType, cameraType)
    }

    fun getCameraType(): String? {
        if (App.cameraType != null) {
            return App.cameraType
        }
        App.cameraType =
            SPUtils.getInstance().getString(CacheK.useCameraType, CommonK.Type_Camera_Back)
        return App.cameraType
    }

    fun saveMediaType(mediaType: String) {
        App.mediaType = mediaType
        SPUtils.getInstance().put(CacheK.useMediaType, mediaType)
    }

    fun getMediaType(): String? {
        if (App.mediaType != null) {
            return App.mediaType
        }
        App.mediaType =
            SPUtils.getInstance().getString(CacheK.useMediaType, CommonK.Type_Media_Photo)
        return App.mediaType
    }


    fun saveAppPassword(password: String) {
        App.appPassWord = password
        SPUtils.getInstance().put(CacheK.appPassword, password)
    }

    fun getAppPassword(): String? {
        if (App.appPassWord != null) {
            return App.appPassWord
        }
        App.appPassWord = SPUtils.getInstance().getString(CacheK.appPassword, "")
        return App.appPassWord
    }


    fun saveVideoMaxDur(videoMaxDur: Int) {
        App.videoMaxDur = videoMaxDur
        SPUtils.getInstance().put(CacheK.videoMaxDur, videoMaxDur)
    }

    fun getVideoMaxDur(): Int? {
        if (App.videoMaxDur != null) {
            return App.videoMaxDur
        }
        App.videoMaxDur = SPUtils.getInstance().getInt(CacheK.videoMaxDur, 15)
        return App.videoMaxDur
    }


    fun saveAudioMaxDur(audioMaxDur: Int) {
        App.audioMaxDur = audioMaxDur
        SPUtils.getInstance().put(CacheK.audioMaxDur, audioMaxDur)
    }

    fun getAudioMaxDur(): Int? {
        if (App.audioMaxDur != null) {
            return App.audioMaxDur
        }
        App.audioMaxDur = SPUtils.getInstance().getInt(CacheK.audioMaxDur, 30)
        return App.audioMaxDur
    }




    fun saveCaptureTime(takeType: String) {
        App.captureTime = takeType
        SPUtils.getInstance().put(CacheK.takeType, takeType)
    }


    fun getCaptureTime(): String? {
        if (App.captureTime != null) {
            return App.captureTime
        }
        App.captureTime = SPUtils.getInstance().getString(CacheK.takeType, CommonK.Type_Home_Key)
        return App.captureTime
    }

    fun saveImageSizeType(imageSizeType: String) {
        App.imageSizeType = imageSizeType
        SPUtils.getInstance().put(CacheK.imageSizeType, imageSizeType)
    }

    fun getImageSizeType(): String? {
        if (App.imageSizeType != null) {
            return App.imageSizeType
        }
        App.imageSizeType =
            SPUtils.getInstance().getString(CacheK.imageSizeType, CommonK.Type_size_1920x1080)
        return App.imageSizeType
    }


    fun saveProExp(exp: Int) {
        val oldExp = getProExp()
        val newExp = oldExp + exp
       SPUtils.getInstance().put(CacheK.proExp, newExp)
    }

    fun getProExp(): Int {
        return SPUtils.getInstance().getInt(CacheK.proExp, 0)
    }


}