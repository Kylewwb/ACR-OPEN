package com.dillon.acr.utils

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.ImageView
import cn.jzvd.Jzvd
import com.blankj.utilcode.util.ScreenUtils
import com.blankj.utilcode.util.Utils
import com.bumptech.glide.Glide
import com.dillon.acr.R
import com.dillon.acr.bean.MediaInfo
import com.dillon.acr.key.CommonK
import com.lxj.xpopup.core.CenterPopupView
import kotlinx.android.synthetic.main.layout_media_popup.view.*

@SuppressLint("ViewConstructor")
class MediaPopup(context: Context, mediaInfo: MediaInfo) : CenterPopupView(context) {
    private var mContext = context
    private var mMediaInfo = mediaInfo

    override fun getImplLayoutId(): Int {
        return R.layout.layout_media_popup
    }

    override fun onCreate() {
        super.onCreate()
        when {
            CommonK.Type_Media_Video == mMediaInfo.type -> {
                lay_video.visibility = View.VISIBLE
                lay_photo.visibility = View.INVISIBLE

                jz_video.clarity.visibility = View.GONE
                jz_video.fullscreenButton.visibility = View.INVISIBLE
                jz_video.replayTextView.visibility = View.INVISIBLE
                jz_video.posterImageView.scaleType = ImageView.ScaleType.CENTER_CROP
                Glide.with(Utils.getApp().baseContext).load(mMediaInfo.file)
                    .into(jz_video.posterImageView)
                jz_video.setUp(mMediaInfo.path, mMediaInfo.title)
                jz_video.startVideoAfterPreloading()

            }
            CommonK.Type_Media_Photo == mMediaInfo.type -> {
                lay_video.visibility = View.INVISIBLE
                lay_photo.visibility = View.VISIBLE
                Glide.with(mContext).load(mMediaInfo.file).into(photo_view)
            }
            else -> {
                //录音文件
                lay_video.visibility = View.VISIBLE
                lay_photo.visibility = View.INVISIBLE

                jz_video.clarity.visibility = View.GONE
                jz_video.fullscreenButton.visibility = View.INVISIBLE
                jz_video.replayTextView.visibility = View.INVISIBLE
                jz_video.setUp(mMediaInfo.path, mMediaInfo.title)
                jz_video.startVideoAfterPreloading()
            }
        }
    }

    override fun onDismiss() {
        super.onDismiss()
        Jzvd.releaseAllVideos()
    }

    override fun getMaxHeight(): Int {
        if (CommonK.Type_Media_Video == mMediaInfo.type || CommonK.Type_Media_Photo == mMediaInfo.type) {
            return (ScreenUtils.getScreenHeight() * 2) / 3
        }
        return (ScreenUtils.getScreenHeight() * 1) / 8
    }

}