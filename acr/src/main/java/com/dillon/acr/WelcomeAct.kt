package com.dillon.acr

import android.Manifest
import android.content.Intent
import com.blankj.utilcode.util.PermissionUtils
import com.dillon.acr.base.BAct
import com.dillon.acr.services.task.RecorderSer

import com.dillon.acr.ui.PermissionAct
import com.dillon.acr.ui.home.HomeAct
import com.dillon.acr.utils.AUtils
import com.dillon.acr.utils.LocalCacheUtils


class WelcomeAct : BAct() {

    override fun initView() {
        release()
        // test()


    }

    private fun release() {

        if (PermissionUtils.isGranted(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.RECORD_AUDIO
            ) && AUtils.checkIgnoreBatteryOptimization() && AUtils.isAccessibilitySettingsOn(
                RecorderSer::class.java
            )
        ) {
            startActivity(Intent(this, HomeAct::class.java))
            finish()
        } else {
            startActivity(
                Intent(
                    this,
                    PermissionAct::class.java
                )
            )
            finish()
        }

    }

    private fun test() {
        startActivity(
            Intent(
                this,
                PermissionAct::class.java

            )
        )
        finish()

    }

}
