package com.dillon.acr.services.task

import com.blankj.utilcode.util.*
import com.dillon.acr.WelcomeAct
import com.dillon.acr.base.BSer
import com.dillon.acr.ui.PermissionAct
import com.dillon.acr.utils.AUtils
import java.util.*


class CheckAccessibilitySer : BSer() {

    private var mTimer: Timer? = null
    private var mTimerTask: TimerTask? = null

    override fun onCreate() {
        super.onCreate()
        startTimer()
    }


    override fun startCommand(): Int {
        return START_STICKY
    }

    private fun startTimer() {
        if (mTimer == null) {
            mTimer = Timer()
        }
        var autoClose = 1
        if (mTimerTask == null) {
            mTimerTask = object : TimerTask() {
                override fun run() {
                    try {
                        if (AUtils.isAccessibilitySettingsOn(RecorderSer::class.java)) {
                            permissionResultBack()
                        } else {
                            autoClose++
                            LogUtils.d(autoClose)
                            if (autoClose > 50) {
                                stopTimer()
                                stopSelf()
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            }

        }
        if (mTimer != null && mTimerTask != null) {
            mTimer?.schedule(mTimerTask, 0, 1500)
        }
    }

    private fun stopTimer() {
        if (mTimer != null) {
            mTimer?.cancel()
            mTimer = null
        }
        if (mTimerTask != null) {
            mTimerTask?.cancel()
            mTimerTask = null
        }
    }


    //获取权限后返回
    private fun permissionResultBack() {
        stopTimer()
        ActivityUtils.startActivity(PermissionAct::class.java)
        stopSelf()
    }


}