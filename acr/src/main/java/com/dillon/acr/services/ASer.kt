package com.dillon.acr.services

import android.content.Intent.*
import android.content.IntentFilter
import com.dillon.acr.base.BSer
import com.dillon.acr.key.ActionK
import com.dillon.acr.receivers.ARec
import com.dillon.acr.services.task.CoreSer
import com.dillon.acr.utils.AUtils


class ASer : BSer() {

    private var aRec: ARec? = null


    private fun registerCoreReceiver() {
        aRec = ARec()
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_SCREEN_ON)
        intentFilter.addAction(ACTION_SCREEN_OFF)
        intentFilter.addAction(ACTION_USER_PRESENT)

        registerReceiver(aRec, intentFilter)
    }

    override fun onCreate() {
        super.onCreate()

        registerCoreReceiver()

    }


    override fun startCommand(): Int {
        AUtils.startCommonService(CoreSer::class.java)
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        if (aRec != null) {
            unregisterReceiver(aRec)

            AUtils.startCommonService(ASerP::class.java)

        }


    }

}