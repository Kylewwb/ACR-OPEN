package com.dillon.acr.services.task

import android.content.Intent
import com.dillon.acr.base.App
import com.dillon.acr.base.BSerIn


class CoreSer : BSerIn() {

    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)
        startAllTask()
    }

    private fun startAllTask() {
        App.pauseTaskSer = false

    }


}
