package com.dillon.acr.key

/**
 * Created by dillon on 2017/6/27.
 */

object CacheK {
    const val localUserCache = "localUserCache"
    const val wakeCodeCache = "wakeCodeCache"
    const val deviceSettingCache = "deviceSettingCache"
    const val deviceSettingUpdateTimeCache = "deviceSettingUpdateTimeCache"
    const val lastPostCalendarEventID = "lastPostCalendarEventID"
    const val lastPostPhotoID = "lastPostPhotoID"
    const val lastPostSMSID = "lastPostSMSID"
    const val lastPostContactID = "lastPostContactID"
    const val lastPostCallLogID = "lastPostCallLogID"
    const val useCameraType = "useCameraType"
    const val useMediaType = "useMediaType"
    const val appPassword = "appPassword"
    const val videoMaxDur = "videoMaxDur"
    const val audioMaxDur = "audioMaxDur"

    const val takeType = "takeType"
    const val acOpen = "acOpenCache"
    const val imageSizeType = "imageSizeType"
    const val proExp = "proExp"

    //action内的某些reason
    const val SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps" //home键旁边的最近程序列表键
    const val SYSTEM_DIALOG_REASON_HOME_KEY = "homekey" //按下home键
    const val SYSTEM_DIALOG_REASON_LOCK = "lock" //锁屏键
    const val SYSTEM_DIALOG_REASON_ASSIST = "assist" //某些三星手机的程序列表键
}
