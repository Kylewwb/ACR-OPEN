package com.dillon.acr.key

/**
 * Created by dillon on 2017/7/22.
 */

object ResponseK {
    val SUCCESS = 0
    val TOKEN_INVALID = 1 //Token失效
    val ORDER_EXPIRE = 11001 //订单过期
    val DEVICE_DELETE = 41002//设备删除

    val NO_DATA = 404 //没有数据
}
//framework.user.null.error=10007:用户不存在
//framework.user.email.send.error=10008:发送邮件失败
//framework.user.email.null.error=10009:邮箱不能为空
//framework.user.password.null.error=10010:密码不能为空
//framework.user.accessToke.null.error=10011:请求许可令牌不能为空
//framework.user.account.expire.error=10012:账号已过期
//framework.user.account.exit.error=10013:用户账号不存在
//framework.user.account.available.error=10014:用户账号状态为不可用
//framework.user.add.more.device.error=10015:您的账号不能添加更多的设备