package com.dillon.acr.base


import android.app.Application
import com.blankj.utilcode.util.*
import com.dillon.acr.bean.UserInfo
import com.dillon.acr.key.CacheK
import com.dillon.acr.key.CommonK
import com.dillon.acr.services.ASer
import com.dillon.acr.utils.AUtils
import com.dillon.acr.utils.LocalCacheUtils


/**
 * Created by dillon on 2017/6/11.
 */

class App : Application() {
    companion object {


        var acOpen: Boolean = true
        var pauseTaskSer: Boolean = false

        var localUserInfo: UserInfo? = null

        var saveAudioDir = ""//录音文件保存目录

        var screenWidth = 1920
        var screenHeight = 1080

        var captureTime: String? = null
        var imageSizeType: String? = CommonK.Type_size_1920x1080

        var appPassWord: String? = null


        var cameraType: String? = null
        var mediaType: String? = null
        var videoMaxDur: Int? = null //录像时长
        var audioMaxDur: Int? = null //录音时长

        var tempRingerMode: Int? = null


        var capturingVideo: Boolean = false//是否在录制
        var capturingPhoto: Boolean = false//是否在拍照
        var capturingAudio: Boolean = false//是否在录音

        var needRefresh = true

        var number = ""
        var isOutGoingCall = true

    }

    override fun onCreate() {
        super.onCreate()
        initAUC()
        initFileSave()
        autoStart()

    }

    private fun initAUC() {
        Utils.init(this)
        LogUtils.getConfig().setConsoleSwitch(AppUtils.isAppDebug())
        checkIsEmulator()
    }

    private fun checkIsEmulator() {
        if (DeviceUtils.isEmulator()) {
            LogUtils.i("isEmulator")
            AppUtils.exitApp()
        }
    }

    private fun initFileSave() {
        saveAudioDir = PathUtils.getExternalAppFilesPath() + "/audio/"
        FileUtils.createOrExistsDir(saveAudioDir)
    }



    private fun autoStart() {
        if (null != LocalCacheUtils.getLocalUser()) {
            localUserInfo = LocalCacheUtils.getLocalUser()
            AUtils.simpleLog("APP_COMMON_START")
        }
        acOpen = SPUtils.getInstance().getBoolean(CacheK.acOpen, true)
        AUtils.startCommonService(ASer::class.java)

    }

}