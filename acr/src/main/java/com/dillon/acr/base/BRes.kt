package com.dillon.acr.base

import java.io.Serializable


/**
 * Created by dillon on 2017/6/16.
 */
//BaseResponse
open class BRes<T> : Serializable {
    var deviceSettingUpdateTime: Long = 0L//配置更新时间标记
    var code: Int = 0//状态码
    var message: String? = null//信息
    var data: T? = null
}
