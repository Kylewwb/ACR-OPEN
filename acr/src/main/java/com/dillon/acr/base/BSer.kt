package com.dillon.acr.base

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.dillon.acr.key.CommonK
import com.dillon.acr.utils.AUtils
import com.blankj.utilcode.util.LogUtils

//BaseService
abstract class BSer : Service() {

    override fun onCreate() {
        super.onCreate()
        LogUtils.v(this.javaClass.simpleName + ": onCreate")
        AUtils.makeNotification(this, CommonK.CHANNEL_ID, CommonK.CHANNEL_NAME)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        LogUtils.v(this.javaClass.simpleName + ": onStartCommand")
        return startCommand()
    }

    abstract fun startCommand(): Int

    override fun onDestroy() {
        super.onDestroy()
        LogUtils.v(this.javaClass.simpleName + ": onDestroy")

    }

}
