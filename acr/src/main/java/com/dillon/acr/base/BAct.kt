package com.dillon.acr.base

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.dillon.acr.utils.AUtils

//BaseActivity
abstract class BAct : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        beforeOnCreateView()
        super.onCreate(savedInstanceState)
        initUi()
        initView()
        initData()
        baseInitData()
        initListener()
    }

    open fun beforeOnCreateView() {}

    open fun initUi() {
        AUtils.appStyle(this)
    }

    open fun initView() {}

    open fun initData() {}

    open fun initListener() {}

    private fun baseInitData() {}

}
