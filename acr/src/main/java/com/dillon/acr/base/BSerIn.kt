package com.dillon.acr.base

import android.app.IntentService
import android.content.Intent
import com.dillon.acr.key.CommonK
import com.dillon.acr.utils.AUtils
import com.blankj.utilcode.util.LogUtils

//BaseIntentService
open class BSerIn : IntentService("BSerIn") {

    override fun onCreate() {
        super.onCreate()
        LogUtils.v(this.javaClass.simpleName + ": onCreate")
        AUtils.makeNotification(this, CommonK.CHANNEL_ID, CommonK.CHANNEL_NAME)
    }

    override fun onHandleIntent(intent: Intent?) {
        LogUtils.v(this.javaClass.simpleName + ": onHandleIntent")
    }

    override fun onDestroy() {
        super.onDestroy()
        LogUtils.v(this.javaClass.simpleName + ": onDestroy")

    }


}
