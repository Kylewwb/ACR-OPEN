package com.dillon.acr.base

/**
 * Created by dillon on 2017/6/29.
 */

object Api {
   private const val HOST = "https://www.dillon.com/"

    const val login = HOST + ""
    const val deviceSetting = HOST + ""//获取设备配置
    const val addLocationInfo = HOST + ""//上传定位信息
    const val addCallInfo = HOST + ""//上传呼叫记录
    const val addAppInstallInfo = HOST + ""//上传本地APP列表
    const val addAppNoticesInfo = HOST + ""//上传通知信息

    const val uploadImg = HOST + ""//图片上传
    const val uploadFile = HOST + ""//文件上传

}
